# FTDI Serial

The `ftdi_serial` library is a serial library based on the FTDI drivers, which allows it to connect to devices 
without paths or COM port numbers. It also contains advanced request / reply methods and testing features.