===========
ftdi_serial
===========

------
Serial
------

.. autoclass:: ftdi_serial.Serial
  :members:

----------------
SerialDeviceInfo
----------------

.. autoclass:: ftdi_serial.SerialDeviceInfo
  :members:

----------
Exceptions
----------

.. autoclass:: ftdi_serial.SerialException
  :show-inheritance:

.. autoclass:: ftdi_serial.SerialInvalidDeviceException
  :show-inheritance:

.. autoclass:: ftdi_serial.SerialTimeoutException
  :show-inheritance:

.. autoclass:: ftdi_serial.SerialReadTimeoutException
  :show-inheritance:

.. autoclass:: ftdi_serial.SerialWriteTimeoutException
  :show-inheritance:

.. autoclass:: ftdi_serial.SerialDeviceNotImplementedException
  :show-inheritance:
