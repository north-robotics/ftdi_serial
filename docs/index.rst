.. ftdi_serial documentation master file, created by
   sphinx-quickstart on Fri Feb 28 11:14:45 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ftdi_serial's documentation!
=======================================

.. toctree::
   :maxdepth: 3

   ftdi_serial

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
