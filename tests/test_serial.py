import unittest
from north_utils.test import assert_equal
from ftdi_serial import Serial

class SerialTest(unittest.TestCase):
    def test_pyserial(self):
        port = Serial.list_device_ports()[0]
        serial = Serial(device_port=port)
        serial.write(b'test')
        data = serial.read(4)
        assert_equal(data, b'test')
        serial.disconnect()

        serial = Serial(port)
        serial.write(b'test')
        data = serial.read(4)
        assert_equal(data, b'test')
        serial.disconnect()

        serial = Serial(port, baudrate=9600)
        serial.write(b'test')
        data = serial.read(4)
        assert_equal(data, b'test')
        serial.disconnect()

        serial = Serial(port, baudrate=9600, parity=Serial.PARITY_ODD, data_bits=Serial.DATA_BITS_7)
        serial.write(b'test')
        serial.flush()
        data = serial.read(4)
        assert_equal(data, b'test')
        serial.disconnect()